# Recommendation APIs
## Giới thiệu
Mô tả các APIs cần dùng trong quá trình xây dựng video recommendation.
### 1. Version
+ api/v1
+ Phần 2: Danh sách APIs yêu cầu phía myclip.vn trả về
    - GET /categories
    - GET /video
    - GET /user
    - logging 
    
+ Phần 3: Danh sách APIs cần trả về cho myclip.vn
    - GET /recommendation/user
    - GET /recommendation/user/relative
    
### 2. Danh sách APIs yêu cầu phía myclip.vn trả về
#### 2.1 GET /categories

- Lấy thông tin chi tiết của tất cả category

##### Response:

```js
{
    data: [
        {
            id: 100,
            name: 'giải trí',
            description:'',
            is_active:true,
            play_times:'',
            play_times_real:'',
            parent_id:''
        },
        {
            id: 101,
            name: 'nhạc',
            description:'',
            is_active:true,
            play_times:'',
            play_times_real:'',
            parent_id:''
        }
    ]
}

``` 
#### 2.2 GET /video

- Lấy thông tin chi tiết của video

##### Parameter:

- video_id = 10000

##### Response:

```js
{   
    id: 10000,    
    name: 'tổng hợp sự khác nhau giữa tết xưa và tết nay',
    description: '',
    slug: 'tong-hop-su-khac-nhau-giua-tet-xua-va-tet-nay',
    category_id: 100,
    published_time: 1550597907,
    is_active: true,
    price_play: '',
    status: '',
    created_at: '10 tháng trước',
    channel_id: 10,
    tag: '',
    play_times: '',
    play_times_real: '',
    like_count: 98,
    dislike_count: 12,
    is_hot: true,
    is_recommend: true,
    duration: '2 phút',
    resolution: ,
    bucket: '',
    path: ''
}

```

#### 2.3 GET /user

- Lấy thông tin chi tiết của user

##### Parameter:

- user_id = 12345

##### Response:

```js
{   
    id: 62626,    
    name: '',
    gender: 'male',
    age: 25,
    address: '',
    phone: '',
    email: '',
    no_followers: 100,
    no_views: 34345,
    no_likes:123,
    no_comment: 423
}

```

#### 2.4 Logging

-  Mô tả các thông tin cần thiết để ghi log hành vi của người dùng trên trang web.

```js
{   
    user_id: 62626,    
    video_id: 10000,
    time_group: {
        time_cookie: 1550597907 ,
        time_create: 1550597907
    },
    current_url: '/video/1431877/clip-tet-xa-nha-cuc-cam-dong',
    referrer_url: 'http://myclip.vn/channel/38210173'
    
}

```

### 3. Danh sách APIs cần trả về cho myclip.vn

#### 3.1 GET /recommendation/user

- Trả về gợi ý top_n video_id cho mỗi người dùng

##### Parameter:

- user_id = 12345


##### Response:

```js
{
    video_ids: [
        1000,
        1001,
        1002,
        1003
    ]   
    
}

```

#### 3.2 GET /recommendation/user/relative

- Trả về gợi ý top_n video_id có liên quan cho mỗi người dùng khi người dùng đang xem một video nào đó.

##### Parameter:

- user_id  = 12345
- video_id = 1000


##### Response:

```js
{
    video_ids: [
        1001,
        1002,
        1003,
        1004
    ]   
    
}

```